<?php

// вивід даних через print
print 'Hello, I\'m not robot, world! <br>';

# вивід строки через echo
echo 'Hello, I\'m not robot, world! <br>';

/*
 * вивід через функцію var_dump
 */
var_dump('Hello, I\'m not robot, world!');

/**
 * вивід даних через var_export
 */
var_export("Hello, I'm not robot, world!");
